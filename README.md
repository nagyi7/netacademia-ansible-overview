# Netacademia Ansible tanfolyam

A bemutatott playbookokat [ebben a repositoryban találjátok](https://bitbucket.org/nagyi7/netacademia-ansible-overview/src)

# Ez előadáson ajánlott linkek

* [NANOG youtube csatorna](https://www.youtube.com/user/TeamNANOG)
* [Kerekasztal beszélgetés hálózati automatizációról](https://www.youtube.com/watch?v=aQFbSovedIE&t=2008s)
* [Ansible dokumentáció](http://docs.ansible.com/)
* [Salt dokumentáció](https://docs.saltstack.com/)
* [Hálózati automatizáció a Cloudflare-nél](https://www.youtube.com/watch?v=99jHvkVM0Dk&t=3217s)
* [NetworkToCode Slack csatorna hálózati témákban](https://slack.networktocode.com)
